#    Linux
# pyinstaller --onefile --windowed $FILENAME.py

#    Windows
# python -m PyInstaller --onefile --windowed $FILENAME.py

# Executable in dist/

from functools import partial

from PyQt5 import QtWidgets as QtW

class Numbers:
    def __init__(self, time, growthp, end_incomep, current_income, displayer=print):
        self.setting = True
        self.time = time
        self.growthp = growthp
        self.end_incomep = end_incomep
        self.current_income = current_income
        self.displayer = displayer
        self.setting = False

    def __setattr__(self, key, value):
        go = False
        if key == 'setting' and value:
            super().__setattr__(key, value)
            self._keys = {'setting', 'contribution'}
            return
        elif getattr(self, 'setting'):
            super().__setattr__(key, value)
            self._keys.add(key)
            if key != 'setting':
                return
        elif key not in self._keys:
            raise KeyError("Unknown key")
        else:
            super().__setattr__(key, value)
        if key != 'contribution':
            self.calculate()
            self.display()

    def calculate(self):
        needed = self.end_incomep / self.growthp
        self.contribution = (needed * self.growthp) / (((self.growthp + 1) ** self.time) - 1)
        return self.contribution

    def display(self):
        yn = self.contribution * self.current_income
        yp = self.contribution * 100
        mn = yn / 12
        mp = yp / 12
        wn = yn / 52
        wp = yp / 52
        dn = yn / 365
        dp = yp / 365
        text = f"""Yearly: {yp:.2f}%, ${yn:.2f}\nMonthly: {mp:.2f}%, ${mn:.2f}
Weekly: {wp:.2f}%, ${wn:.2f}\nDaily: {dp:.2f}%, ${dn:.2f}"""
        self.displayer(text)


class MainWindow(QtW.QMainWindow):
    def __init__(self, time=40, growthp=10, endp=80, current=50):
        super().__init__()
        self.label = QtW.QLabel()
        self.numbers = Numbers(time, growthp/100, endp/100, current*1000, self.label.setText)
        self.setWindowTitle("Contribution Calculator")
        self.layout = QtW.QVBoxLayout()
        self.income = self.add_spin("Income:", 10, 200)
        self.income.setValue(current)
        self.income.setSingleStep(5)
        self.income.setPrefix("$")
        self.income.setSuffix(",000")
        self.time = self.add_spin("Years investing:", 1, 80)
        self.time.setValue(time)
        self.growth = self.add_spin("Annual growth:", 1, 25)
        self.growth.setSuffix("%")
        self.growth.setValue(growthp)
        self.end = self.add_spin("Retirement income:", 50, 100)
        self.end.setSingleStep(5)
        self.end.setSuffix("%")
        self.end.setValue(endp)
        self.layout.addWidget(self.label)
        widget = QtW.QWidget()
        widget.setLayout(self.layout)
        self.setCentralWidget(widget)

    def add_spin(self, label, min, max):
        self.adding = True
        box = QtW.QHBoxLayout()
        box.addWidget(QtW.QLabel(label))
        spin = QtW.QSpinBox()
        spin.valueChanged.connect(partial(self.spin_change, spin))
        spin.setMinimum(min)
        spin.setMaximum(max)
        box.addWidget(spin)
        self.layout.addLayout(box)
        self.adding = False
        return spin

    def spin_change(self, spin, value):
        if self.adding:
            return
        if spin is self.income:
            self.numbers.current_income = spin.value() * 1000
        elif spin is self.time:
            self.numbers.time = spin.value()
        elif spin is self.growth:
            self.numbers.growthp = spin.value() / 100
        elif spin is self.end:
            self.numbers.end_incomep = spin.value() / 100

if __name__ == '__main__':
    app = QtW.QApplication(['ContriCalc'])
    window = MainWindow()
    window.show()
    app.exec()
